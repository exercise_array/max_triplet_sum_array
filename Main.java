import java.util.Arrays;

public class Main {

    public static int maxSumTriplet(int[] arr, int n) {
        int sum = Integer.MIN_VALUE;
        for (int i = 0; i < n - 2; i++) {
            for (int j = i + 1; j < n - 1; j++) {
                for (int k = j + 1; k < n; k++) {
                    if (sum < arr[i] + arr[j] + arr[k])
                        sum = arr[i] + arr[j] + arr[k];
                }
            }
        }
        return sum;
    }

    public static int maxSumTripleSorted(int[] arr, int n) {
        Arrays.sort(arr);
        return arr[n - 1] + arr[n - 2] + arr[n - 3];
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 7, 2, 10, 5};
        System.out.println(maxSumTriplet(arr, arr.length));
        System.out.println(maxSumTripleSorted(arr, arr.length));
    }
}
